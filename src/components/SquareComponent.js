import React from 'react';

export const SquareComponent = (props) => {

	let insertNumber = (props.insertNumber === 0)? '': props.insertNumber;

	return <div className="Square"><input className="inputSquare" value={insertNumber} type="text" maxLength="3" key={props.key} /></div>;
};
