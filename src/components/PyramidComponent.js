import React from 'react';
import { SquareComponent } from './SquareComponent';

export const PyramidComponent = () => {
	const totalRows = 5;
	let pyramid = [], index = 0;
	const prefillNumbers = randomNumbers();


	for (let i = 0; i < totalRows; i++) {
		let row = [];
		let randomNumber;



		for (let j = 0; j < i + 1; j++) {
			randomNumber = 0;

			index++;

			if (index === 4 || index === 10) {
				randomNumber = prefillNumbers.pop()
			}

			row.push(<SquareComponent insertNumber={randomNumber} key={index}></SquareComponent>);
		}
		pyramid.push(row);
		pyramid.push(<br key={`br_` + index}/>);
	}

	return (
		<div className="pyramidWrapper">
			{pyramid}
		</div>
	);
};

const randomNumbers = (count = 2) => {
	let randNumbers = [];

	for (let i = 1; i <= count; i++) {
		// create random integer between 0 and 100
		randNumbers.push(Math.floor(Math.random() * 101));
	}

	return randNumbers.sort((a, b) => a - b);
};
