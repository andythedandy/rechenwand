import React from 'react';
import { PyramidComponent } from './components/PyramidComponent';
import './App.css';

function App() {
	return (
		<div className="App">
			<h1>Rechenwand</h1>
			<PyramidComponent/>
		</div>
	);
}

export default App;
